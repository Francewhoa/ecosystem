# The Onion Services Ecosystem

This [Onion Services][] Ecosystem Documentation aggregates content from many
projects into a single searchable website for the wider Tor community.

<div class="grid cards" markdown>

-   :onion:{ .lg .middle } __Technology__

    ---

    Discover the Onion Services technology.

    [:octicons-arrow-right-24: Technology docs](technology/README.md)

-   :tools:{ .lg .moddle } __Applications__

    ---

    Check the documentation for existing Onion Service applications.

    [:octicons-arrow-right-24: Application docs](apps/README.md)

-   :material-developer-board:{ .lg .middle } __Development__

    ---

    Specific development docs are intended to be available in the upcoming [Tor
    Developer Portal][]. While this not happens, a temporary location for dev
    docs is available here.

    [:octicons-arrow-right-24: Developer docs](dev/README.md)

-   :test_tube:{ .lg .middle } __Research__

    ---

    Onion Services research is documented at the Onion Plan.

    [:octicons-arrow-right-24: Research docs](research/README.md)

</div>

[Onion Services]: https://community.torproject.org/onion-services/
[Tor Developer Portal]: https://gitlab.torproject.org/groups/tpo/-/milestones/23
