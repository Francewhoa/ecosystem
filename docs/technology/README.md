# Technology

[Onion Services][] technology is a communication layer offering many security and
availability properties.

It's currently defined in the [Tor Rendezvous Specification Version 3][], and this
section has further discussions about how it works and it's properties.

Check the [Introduction to Onion Services][] slide deck (from 2022) for a
technology and applications overview.

[Tor Rendezvous Specification Version 3]: https://spec.torproject.org/rend-spec/index.html
[Onion Services]: https://community.torproject.org/onion-services/
[Introduction to Onion Services]: https://gitlab.torproject.org/tpo/community/training/-/blob/master/2022/onion-services/intro-onion-service-2022.pdf
