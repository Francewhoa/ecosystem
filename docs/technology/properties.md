# Onion Services Properties

<!--
  Arguments gathered by @raya, @rhatto and others at
  https://gitlab.torproject.org/tpo/onion-services/onion-support/-/issues/166
-->

[Onion Services][] have many interesting and useful properties, making it a
good fit for privacy-preserving applications.

[Onion Services]: https://community.torproject.org/onion-services/

## Summary

The table below offers a good summary, comparing regular websites, regular
websites over Tor, and Onion Services (check the [Terminology](terminology.md)
page for definitions such as "Regular website"):

| Property | Regular website  | Regular website over Tor  | Onion Service  |
| ------   | ------           | ------                    | ------         |
| Censorship resistance | Poor: Website can easily be censored | Good: Censorship still possible via exit nodes | Very good: Accessible as long as Tor is reachable, address not censorable |
| Privacy safeguards    | Very low: only minimal safeguards via HTTPS, susceptible to traffic surveillance   | Good: Data correlation is not an eliminated risk | Very good: End-to-end encryption for user and service, anonymity for both |
| Metadata elimination  | Poor: Data about online activity recorded by websites and entities passing traffic | Good: Data about online activity can be recorded by website if user logs in and identifies themselves | Very good: Metadata logging eliminated on both ends, but website can record data if user logs in |

Some of these properties arise from the addressing mechanisms built in the
technology (The Onion Space), while others comes from the way clients connects
to services (The Rendezvous).

So, roughly speaking, the technology can be explained in a higher level as

    Onion Services = The Onion Space + The Rendezvous

Applications can them benefit from these addressing and routing systems along
with other protocols (such as HTTPS and SSH).

## The Onion Space

### What is the Onion Space?

As explained in the [Onion Services specification][], an .onion address is
basically a random-looking number that is used as a public key.

The Onion Space consists in all possible .onion addresses, and this number is
huge (in the order of $2^{251}$ possible addresses, check the discussion below),
which is bigger than the estimated number of atoms in the galaxy[^atoms].

This does not mean that there are so many running Onion Services out there.
What is important is that:

1. The _actual_ number of Onion Services should be way smaller than the total
   number of _possible_ Onion Services.
2. The number of _possible_ Onion Services should be so big that everyone can
   have as many addresses as needed and use them to
   [chat](../apps/messaging/README.md), to [share
   files](../apps/sharing/README.md) or even [running an onionsite](../apps/web/README.md),
   even if they just use an address only once and then discard it.

[^atoms]: For the comparison with the estimated number of atoms in the galaxy, check
  [atoms in the galaxy](https://www.wolframalpha.com/input?i=atoms+in+the+galaxy),
  [4.6×10^68 in base 2](https://www.wolframalpha.com/input?i=4.6%C3%9710%5E68+in+base+2)
  and [2^228](https://www.wolframalpha.com/input?i=2%5E228).

[Connecting to an Onion Service][] address means using the .onion address
through the Tor network to establish a connection to the remote computer that
has control over the corresponding .onion address' private key.

[Onion Services specification]: https://spec.torproject.org/rend-spec-v3
[Connecting to an Onion Service]: https://community.torproject.org/onion-services/overview/

### The Onion space is a border-less, country-agnostic space

__Onion Services offer a country-agnostic Web experience__. You're not visiting
an onionsite in/from a specific country, you're simply visiting a website in a
virtual, border-less, protected space.

When accessing a website over VPN, you're exiting from the VPN server which is
located in a specific country, and the experience of navigating that website
will differ based on which country you're visiting it from (i.e.  where the VPN
server is located). When accessing a website over Tor, this depends on where
the exit node is located.

When accessing an Onion Service on the other hand, you're not "exiting" from
anywhere, rather you're meeting the website inside the Tor network. The
communication between you and the website is also protected by end-to-end
encryption.

There are positives to this (not necessarily technical – rather affective):

1. A country-agnostic visit implies one that removes risks and pressures put
   forward by different jurisdictions (similar to what VPN providers and Tor
    exit nodes face).
2. A border-less Internet is akin to a quiet and peaceful room away from the
   noise bustling outside of the Tor network.

While this argument can easily be used to idealize Onion Services, it's not the
aim here.

### The Onion Space Size

The Onion Services version 3 have, in theory, 256 bits of space size, but the
actual number is a little smaller, since not any possible 256 bit number can be
a valid public key: Tor clients should check if the key has a "torsion
component", as described at [Client-side validation of onion addresses][]:

> As part of the address validation, Tor clients should check that the
> underlying ed25519 key does not have a torsion component. If Tor accepted
> ed25519 keys with torsion components, attackers could create multiple
> equivalent onion addresses for a single ed25519 key, which would map to the
> same service. We want to avoid that because it could lead to phishing
> attacks and surprising behaviors (e.g. imagine a browser plugin that blocks
> onion addresses, but could be bypassed using an equivalent onion address
> with a torsion component).

This implies that not all of the possible 256 bit combinations can be used as
public keys, but a slightly smaller number: it's estimated that this makes the
space around 5 bits smaller, having a total keyspace in the order of
$2^{256-x}$ for $x \approx 5$.

That still means that allocation space it's still bigger (almost twice)
the size of the [IPv6][] space (which supports up to 128 bits of address space)!

With onion service public keys, with the torsion component taken into account,
the key clamping, etc. would you still say the key space for onion keys are
around the size of $2^{256}$ or would you say it's smaller?

[Client-side validation of onion addresses]: https://spec.torproject.org/rend-spec/deriving-keys.html?highlight=torsion#addr-validation
[IPv6]: https://en.wikipedia.org/wiki/IPv6

### Enumerating / brute forcing the Onion Space

Besides being huge, it's not easy to enumerate the Onion Space[^forum-thread].

With the arrival of version 3 Onion Services, Tor added a protection mechanism
to the Onion Services subsystem in Tor that prevents relays from enumerating
which Onion Services they may be involved with providing access to.

This means there is no longer an efficient way of enumerating Onion Services by
participating in the Tor network as a relay. This property of version 3 Onion
Services is considered a feature -- not a bug, but it makes it harder for you
to achieve your goal.

You will have a bad time enumerating these addresses using brute force. The
Onion Service addresses contain a 256-bit ed25519 public key encoded in the
“.onion” hostname. Brute forcing thus means you must enumerate possible all
values from 0 to $2^{256}$, which is an incomprehensibly large number of
addresses, with only a tiny fraction being actual, available, Onion Services.
Remember, IPv6 is “only” $2^{128}$ addresses available globally, and that is
already a vast number of addresses.

I would guess the way others build their Onion Services index is likely from
scanning for valid ".onion" strings in various public data sets on the
internet. Thus slowly building up a corpus of available Onion Services.

[^forum-thread]: This subsection originally appeared in the forum thread [Is
  there an index for onion domains?][], and here is presented with a few
  changes.

[Is there an index for onion domains?]: https://forum.torproject.net/t/is-there-an-index-for-onion-domains/6893/3

## The Rendezvous

The "rendezvous", [in the Tor jargon][], is a connection to an arbitrarily
chosen Tor node between a client and an Onion Service.

This connection is done through a [special way][] that offers many security and
privacy properties.

[in the Tor jargon]: https://spec.torproject.org/rend-spec/rendezvous-protocol.html?highlight=rendezvous#the-rendezvous-protocol
[special way]: https://community.torproject.org/onion-services/overview/

### Censorship resistance

* Both location and IP address of an Onion Service are hidden, making it
  difficult to censor or identify who runs the service.
* As a result they used to be called “hidden services”.
* Tor exit nodes can block websites (rare), Onion Services never exit the Tor
  network.
* It’s the most censorship-resistant technology available out there, as long as
  the Tor network is reachable to the user.

### Metadata obfuscation and elimination

* When you use the Tor network to browse the Web you are not sending any
  information by default of who you are or where you are connecting from.
* The Onion Services use the Tor network to eliminate information about where
  they are situated.
* Using them eliminates all metadata that may be associated with the service
  otherwise.

### End-to-end encryption between user and website

* Furthering the protection of sources and whistleblowers.

### Contributing to the decentralized Web

* To deploy an Onion Service, you don't need a static or dedicated IP address
  nor need to purchase a domain and submit it for approval.
* Deployment is easy: you don't need to forward ports or configure your modem.
* For smaller websites like blogs, there’s no need for expensive hardware.

### Maximum harm reduction

* Leaving the Tor network still puts users at risk of censorship and other
  security and privacy risks, Onion Services almost diminish these risks.
* Even if websites are under DDoS (Distributed Denial of Service), Onion
  Services could still give access to content of the site (in the case that the
  onion service itself is not under DDoS (but [there are protections if
  that happens](pow.md)!).
* Visiting an Onion Service is more secure than visiting a regular website over
  Tor.
* There are multiple cases of people prosecuted over digital evidence -
  particularly metadata and IP info - that can only be avoided if people used
  Onion Services.

### Tor network sustainability

The traffic generated by Onion Services doesn't leave the Tor network, and
therefore, these onion circuits free up exit relay bandwidth for others. This
is important because exit relays are a limited resource, making up 20% of the
7000 relays (as of November 2022). As they're a small fraction of the network,
in general, exit relays are overloaded and represent a bottleneck for Tor
users' browsing experience.

## Additional arguments

Here we gather other lists of properties and arguments in favour of Onion
Services:

* [Why build an Onion Site / use Onion Services?](https://github.com/alecmuffett/guide-to-onion-services)
* [Onion Plan's Preamble](../research/preamble/challenges.md).

## Notes
