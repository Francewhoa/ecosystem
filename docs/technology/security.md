# Security Overview

In this page we document existing risk analysis and threat models about the
Onion Service technology itself or from other tools relying on it.

* [Onion Service security overview at the vanguards documentation](https://github.com/mikeperry-tor/vanguards/blob/master/README_SECURITY.md).
* [OnionShare 2.6 Security Design](https://docs.onionshare.org/2.6/en/security.html).
* [Briar Threat Model](https://code.briarproject.org/briar/briar/-/wikis/threat-model).
* [Oniongroove Threat Model](https://tpo.pages.torproject.net/onion-services/oniongroove/threat/).
* [Onionspray security pages](../apps/web/onionspray/security/README.md).
* [Quiet Threat Model](https://github.com/TryQuiet/quiet/wiki/Threat-Model).
* [Proposal 344][] deals with protocol information leaks in Tor, and some of those may affect Onion Services.

[Proposal 344]: https://spec.torproject.org/proposals/344-protocol-info-leaks.html
