# Terminology

This page tracks the terminology when referring to Onion Services.

## Definitions

* Regular internet, or sometimes referred as "clear net": which means
  someone is connecting to remote endpoints on the internet without
  using the Tor network.

* Regular site: a site whose name is available through the DNS (Domain Name
  System). This usually means that someone is accessing a website by it's DNS
  domain name and without using the Tor network. This might be ambiguous
  sometimes, and can refer either to a website that does not have it's .onion
  counterpart, or just that the user is not accessing the website through Tor.

* Regular site over Tor: it's a website whose domain name is available in
  the DNS, but that's being accessed through the Tor network.

* Onionsite: it's a website whose address is an .onion address and that is
  reachable only through the Tor network. This also might be a bit ambiguous
  as sometimes a website can be accessed both as a _regular site_ (through
  a DNS-based domain name) and as an _onionsite_ (through a .onion address
  published in the Tor network).

* Censorship resistance: when content cannot be removed from the internet and
  usually referred to an Onion Service site; this is different from Tor
  bridges, that offers _censorship circumvention_ that allows users to connect to
  the Tor network.

## Recommendations

* "Onion Service" and "Onion Services" instead of "onion service"
  (recommendation to capitalize where applicable).
* Use of "onionsite" instead of constructions like ".onion site".

## References

* [Onion Services Terminology](https://gitlab.torproject.org/tpo/web/community/-/issues/60)
* [Onion Services terminology - second iteration](https://gitlab.torproject.org/tpo/onion-services/onion-support/-/issues/88)
* [Tor Rendezvous Specification - Version 3](https://spec.torproject.org/rend-spec-v3).
