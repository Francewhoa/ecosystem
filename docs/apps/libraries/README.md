# Libraries

Onion Service libraries:

* [Gosling](http://gosling.technology/): a library that allows developers to
  create applications which provide anonymous, secure, and private peer-to-peer
  functionality using Tor onion services.
* [Cwtch: Decentralized, Surveillance Resistant
  Infrastructure](https://cwtch.im/): a decentralized, privacy-preserving,
  multi-party messaging protocol that can be used to build metadata resistant
  applications.
* [Stem tutorial for Onion Services](https://stem.torproject.org/tutorials/over_the_river.html)
* [txtorcon Onion Services documentation](https://txtorcon.readthedocs.io/en/latest/guide.html#onion-hidden-services)
