# Deployment tools

This is a comprehensible yet incomplete curated list of deployment tools for
Onion Service sites.

It covers some of the most relevant tools to be considered and was initially
compiled at [tpo/onion-services/oniongroove#1][].

Projects that were too basic or evidently unmaintained were removed from the
result set.

[tpo/onion-services/oniongroove#1]: https://gitlab.torproject.org/tpo/onion-services/oniongroove/-/issues/1

## Command line

* [GitHub - alecmuffett/eotk: Enterprise Onion Toolkit](https://github.com/alecmuffett/eotk#readme) (inspired by [the deployment experience at ProPublica](https://www.propublica.org/nerds/a-more-secure-and-anonymous-propublica-using-tor-hidden-services))
* [Hiro / Roid · GitLab](https://gitlab.torproject.org/hiro/roid)

## [Ansible](https://www.ansible.com/ "Ansible is Simple IT Automation")

* [systemfailure.net/ansible\_onion\_services: Create and manage Onion services with ansible - ansible\_onion\_services - Codeberg.org](https://codeberg.org/systemfailure.net/ansible_onion_services)
* [GitHub - systemli/ansible-role-onion: Install and configure Tor Onion Services](https://github.com/systemli/ansible-role-onion)
* [GitHub - AnarchoTechNYC/ansible-role-tor: Securely build a system Tor and optionally configure numerous high-security Onion services.](https://github.com/AnarchoTechNYC/ansible-role-tor)
* [Armando Lüscher / ansible-role-tor · GitLab](https://gitlab.com/noplanman/ansible-role-tor)

## [Terraform](https://www.terraform.io/ "Terraform by HashiCorp")

* [Hiro / Roid · GitLab](https://gitlab.torproject.org/hiro/roid)

## [Python](https://www.python.org/ "Welcome to Python.org")

* [GitHub - nm17/onionify: Create and manage onion services and more in one CLI command](https://github.com/nm17/onionify)
* [GitHub - wybiral/shh: Create Tor hidden services in Python.](https://github.com/wybiral/shh)
* [GitHub - satwikkansal/tor-hidden-service-python: A very basic project creating a `.onion` website for Tor using Flask framework and python.](https://github.com/satwikkansal/tor-hidden-service-python)

## [Puppet](https://puppet.com/ "Powerful infrastructure automation and delivery | Puppet"):

* [smash/tor · Installs, configures and manages Tor · Puppet
  Forge](https://forge.puppet.com/modules/smash/tor) (seems like had
  Onionbalance support in the past, [but that was
  dropped at some point](https://gitlab.com/shared-puppet-modules-group/tor/-/commits/master?search=balance))

## [Heroku](https://www.heroku.com/ "Cloud Application Platform | Heroku")

* [GitHub - aurorafossorg/heroku-buildpack-tor: Run a tor hidden service on Heroku](https://github.com/aurorafossorg/heroku-buildpack-tor)
* [Hiro / Roid · GitLab](https://gitlab.torproject.org/hiro/roid)
* [Hiro / onions on heroku · GitLab](https://gitlab.torproject.org/hiro/onions-on-heroku)

## [Docker](https://www.docker.com/)

* [GitHub - opsxcq/docker-tor-hiddenservice-nginx: Easily setup a hidden service inside the Tor network](https://github.com/opsxcq/docker-tor-hiddenservice-nginx)
* [GitHub - nexus-uw/dockerswarm-hidden-service: docker image for hosting a tor hidden service on docker swarm](https://github.com/nexus-uw/dockerswarm-hidden-service)
* [GitHub - kpetrilli/DWHost: Docker TOR simple hidden webserver](https://github.com/kpetrilli/DWHost "GitHub - kpetrilli/DWHost: Docker TOR simple hidden webserver")
* [GitHub - mcat-ee/onionbalance-docker: A docker image running the Onionbalance hidden service load balancer](https://github.com/mcat-ee/onionbalance-docker)
* [GitHub - torservers/onionize-docker: Tor v3 onion services (hidden services) for Docker containers](https://github.com/torservers/onionize-docker)
* [GitHub - PurpleBooth/tor-hidden-service: Simple Tor container to make a service available over tor.](https://github.com/PurpleBooth/tor-hidden-service)
* [GitHub - HarshVaragiya/TOR-Microservice: An experiment to distribute a tor hidden service into multiple microservices all of which run on tor](https://github.com/HarshVaragiya/TOR-Microservice)
* [GitHub - Yanik39/TORNet: Full featured web server for TOR Hidden Services with Vanguards, NGINX, PHP-FPM, MariaDB, NYX, Supervisor and dnsmasq. One Container for all.](https://github.com/Yanik39/TORNet)
* [GitHub - mcat-ee/onionbalance-docker: A docker image running the Onionbalance hidden service load balancer](https://github.com/mcat-ee/onionbalance-docker)
* [GitHub - nexus-uw/dockerswarm-hidden-service: docker image for hosting a tor hidden service on docker swarm](https://github.com/nexus-uw/dockerswarm-hidden-service)
* [GitHub - PurpleBooth/tor-hidden-service: Simple Tor container to make a service available over tor.](https://github.com/PurpleBooth/tor-hidden-service)
* [goldy/tor-hidden-service - Docker Image | Docker Hub](https://hub.docker.com/r/goldy/tor-hidden-service)

## [Kubernetes](https://kubernetes.io/ "Kubernetes")

* [GitHub - bugfest/tor-controller: Run Tor onion services on Kubernetes (actively maintained)](https://github.com/bugfest/tor-controller)
* [GitHub - agabani/tor-operator: Tor Operator is a Kubernetes Operator that manages Onion Balances, Onion Keys and Onion Services to provide a highly available, load balanced and fault tolerate Tor Ingress and Tor Proxy](https://github.com/agabani/tor-operator) ([documentation](https://agabani.github.io/tor-operator/docs/)).
* [ValtteriL/OnionFermenter: A tool for creating bitcoin stealing phishing clones of onion services on large scale](https://github.com/ValtteriL/OnionFermenter):
  this is as a proof of concept showing an attack vector against users that
  don't properly check Onion Service address, but it's in this list as a
  Kubernetes deployment example.

## [Onionbalance](https://tpo.pages.torproject.net/onion-services/onionbalance):

Different projects dealing with Onionbalance deployment:

* [GitHub - spreadspace/onionbalance-docker: Container for running onionbalance with kubernetes-managed services](https://github.com/spreadspace/onionbalance-docker)
* [GitHub - nogoegst/avant: simple and fast onion balancing (a la onionbalance)](https://github.com/nogoegst/avant)
* [GitHub - FriendlyAdmin/onionbalance: Onionbalance and Tor neatly packaged in a single Docker image.](https://github.com/FriendlyAdmin/onionbalance)
* [GitHub - mcat-ee/onionbalance-docker: A docker image running the Onionbalance hidden service load balancer](https://github.com/mcat-ee/onionbalance-docker)
* [GitHub - FedericoCeratto/debian-onionbalance: Mirror copy of the Debian packaging of onionbalance](https://github.com/FedericoCeratto/debian-onionbalance)
* [GitHub - Hoffamania/onionbalance-helpers: A collection of helpful snippets for one who wishes to experiment with onion balance](https://github.com/Hoffamania/onionbalance-helpers)
* [GitHub - FriendlyAdmin/tor-hs: An easy way to host your Tor hidden service.](https://github.com/FriendlyAdmin/tor-hs)
