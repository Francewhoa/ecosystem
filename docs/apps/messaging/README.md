# Messaging

Onion Services-based messaging applications:

* [Briar: Secure messaging, anywhere - Briar](https://briarproject.org/) and [Briar Desktop](https://code.briarproject.org/briar/briar-desktop).
* [OnionShare](https://onionshare.org/) is primarily a file sharing tool, but also has chat functionality.
* [Quiet - Private messaging. No servers.](https://tryquiet.org/)
* [Ricochet Refresh: Anonymous peer-to-peer instant messaging](https://www.ricochetrefresh.net/)
