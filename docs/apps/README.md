# Intro

There are many applications already benefiting from Onion Services.

!!! note

    Not all apps mentioned in this section are maintained, developed or
    officially endorsed by the [Tor Project][].

[Tor Project]: https://torproject.org

<div class="grid cards" markdown>

-   :material-web:{ .lg .moddle } __Web__

    ---

    Bringing Onion Services technology for websites.

    [:octicons-arrow-right-24: Web docs](web/README.md)

-   :material-chat-processing:{ .lg .middle } __Messaging__

    ---

    Messaging applications powered by Onion Service technology.

    [:octicons-arrow-right-24: Messaging docs](messaging/README.md)

-   :material-share-variant:{ .lg .middle } __Sharing__

    ---

    Sharing applications with Onion Services support.

    [:octicons-arrow-right-24: Sharing docs](sharing/README.md)

-   :toolbox:{ .lg .middle } __Other__

    ---

    Other applications using Onion Services.

    [:octicons-arrow-right-24: Other docs](other/README.md)

-   :octicons-tools-24:{ .lg .middle } __Libraries__

    ---

    Onion Services libraries.

    [:octicons-arrow-right-24: Libraries docs](libraries/README.md)

</div>
