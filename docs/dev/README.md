# Development

This section has basic information about how to develop applications with
buil-in Onion Services support, and it's intended to be replace in the future
with a proper section in the upcoming [Tor Developer Portal][].

* If you want quick examples about Onion Services integration, look at the
  [Cebollitas][] project.
* Another interesting integration project is the [Onion Desktop][].
* More experienced developers might be interested in know which
  [libraries](../apps/libraries/README.md) are available.

[Tor Developer Portal]: https://gitlab.torproject.org/groups/tpo/-/milestones/23
[Cebollitas]: https://gitlab.torproject.org/tpo/onion-services/cebollitas
[Onion Desktop]: https://gitlab.torproject.org/tpo/onion-services/onion-desktop
