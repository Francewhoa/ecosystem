# The Onion Services Ecosystem

## About

This [Onion Services][] Ecosystem Documentation aggregates content from many
projects into a single searchable website.

Browse it [here][].

## How it works

* It has some documentation on it's own directly in the [docs/](docs) folder.
* But it also fetches documentation from projects which are already using
  [Onion MkDocs][], pulling then into the `repos/` folder, and assembling
  everything in a single searchable site. Check the [compilation script][] for
  how it works.
* A [scheduled pipeline][] ensures that the site stays with fresh content
  from the other projects.

[Onion Services]: https://community.torproject.org/onion-services/
[here]: https://tpo.pages.torproject.net/onion-services/ecosystem/
[Onion MkDocs]: https://gitlab.torproject.org/tpo/web/onion-mkdocs
[compilation script]: scripts/compile
[scheduled pipeline]: https://gitlab.torproject.org/tpo/onion-services/ecosystem/-/pipeline_schedules
